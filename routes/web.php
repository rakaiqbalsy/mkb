<?php

use App\Http\Controllers\AuthControllers;
use App\Http\Controllers\DashboardControllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthControllers::class, 'login'])->name('login');
Route::post('actionlogin', [AuthControllers::class, 'actionLogin'])->name('actionlogin');

Route::group(['middleware' => 'auth'], function () {
    Route::get('actionlogout', [AuthControllers::class, 'actionLogout'])->name('actionlogout');

    Route::get('dashboard', [DashboardControllers::class, 'index'])->name('dashboard');
});

